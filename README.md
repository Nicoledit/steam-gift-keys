# Steam gift keys

Duplicate games and programs I have on Steam and other sites. I gift them to those who are interested.

## Limits

To avoid abuse, I will only give one key per account and per day.  
I would like to be able to give them to the maximum number of people, so first come, first served will not be automatic (_don't be angry with me_ 🥺)

## Getting started

Just go to issues section and check available games:

- Open issue = gift code {+ available +}
- Closed issue = gift code {- no longer available -}

Title of the issue = Game Name

Available labels:
- DLC / Game / Program
- Linux / Mac / Windows
- Not on Steam / On Steam

## Request a key

Leave a comment in the issue of the game you want or contact me on [Mastodon](https://framapiaf.org/@Nicoledit)

I do not guarantee at all provided keys will work (outdated, already used...) and I'm not here to do support, but just to give games/programs 😉
